﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour {

	public float life;
	public float burstSpeed;
	private float counter;
	public float angle;
	public GameObject cube;
	// Use this for initialization
	void Start () {
		life = 10;
		burstSpeed = 5;
	}
	
	// Update is called once per frame
	void Update () {
		life -= 1*Time.deltaTime;
		counter += 10*Time.deltaTime;
		angle = (transform.rotation.eulerAngles.y*-1) * (Mathf.PI / 180);

		if (burstSpeed > 2) burstSpeed -= 2*Time.deltaTime;
		else burstSpeed = 2;

		float X = (Mathf.Cos(angle)*(burstSpeed*2))*Time.deltaTime;
			float Z = (Mathf.Sin(angle)*(burstSpeed*2))*Time.deltaTime;

		transform.position += new Vector3(X, 0, Z);

		float pulse = .75f + (Mathf.Sin(counter)*.2f);
		cube.transform.localScale = new Vector3(pulse,pulse,pulse);

		if (life <= 0) Destroy(this.gameObject);
	}
}
