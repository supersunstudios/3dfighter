﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {

	public Player player;
	public RectTransform bar;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		float val = player.life / player.lifeMax;
		if (val < 0) val = 0;
		bar.transform.localScale = new Vector3(val,1,1);
	}
}
