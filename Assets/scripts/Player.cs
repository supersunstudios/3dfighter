﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	public int playerID;
	private int side;
	public float life = 10;
	public float lifeMax = 10;
	public float distance = 0;
	public float angle = 0;
	public Vector3 pos;
	public Transform target;
	public GameObject bulletPrefab;
	private string ctrl_left;
	private string ctrl_right;
	private string ctrl_up;
	private string ctrl_down;
	private string ctrl_shoot;

	// Use this for initialization
	void Start () {
		pos = this.transform.position;

		switch (playerID) {
			case 1:
				side = -1;
				ctrl_left = "a";
				ctrl_right = "d";
				ctrl_up = "w";
				ctrl_down = "s";
				ctrl_shoot = "q";
			break;
			case 2:
			side = 1;
				ctrl_left = "left";
				ctrl_right = "right";
				ctrl_up = "up";
				ctrl_down = "down";
				ctrl_shoot = "/";
			break;
		}	
	}
	
	// Update is called once per frame
	void Update () {
		Movement();
		Shoot();
	}

	void Movement () 
	{
		distance = Distance2D(transform.position.x, target.position.x, transform.position.z, target.position.z);
		angle = Angle2D(transform.position.x, target.position.x, transform.position.z, target.position.z);

		int xDir = 0;
		int yDir = 0;
		float X;
		float Z;
		float rads;

		if (Input.GetKeyDown(ctrl_left)) xDir = -1;
		else
		if (Input.GetKeyDown(ctrl_right)) xDir = 1;
		
		if (Input.GetKeyDown(ctrl_up)) yDir = 1;
		else
		if (Input.GetKeyDown(ctrl_down)) yDir = -1;
		
		if (xDir != 0) {
			rads = angle * (Mathf.PI / 180);
			distance += (xDir*2)*side;

			X = target.position.x + distance * (Mathf.Cos(rads));
			Z = target.position.z + distance * (Mathf.Sin(rads));

			//transform.position = new Vector3(X, 1, Z);
			pos = new Vector3(X, 1, Z);
		}

		if (yDir != 0) {
			angle += (yDir*side*135)/distance;
			rads = angle * (Mathf.PI / 180);

			X = target.position.x + distance * (Mathf.Cos(rads));
			Z = target.position.z + distance * (Mathf.Sin(rads));

			//transform.position = new Vector3(X, 1, Z);
			pos = new Vector3(X, 1, Z);
		}

		transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 180-angle, 0), .25f);
		transform.position = Vector3.Lerp(transform.position, pos, .25f);
	}

	void Shoot() {

		if (Input.GetKeyDown(ctrl_shoot)) {
			GameObject bullet = GameObject.Instantiate(bulletPrefab, transform.position + transform.right*1f, transform.rotation);
		}

	}

	void OnCollisionEnter(Collision col) {
		bullet b = col.gameObject.GetComponent<bullet> ();
		if (b != null) {
			life --;
			Destroy (col.gameObject);
		}
	}

	float Distance2D (float x1, float x2, float y1, float y2) {
		float dx = x1 - x2;
		float dy = y1 - y2;
		return Mathf.Sqrt(dx * dx + dy * dy);
	}
	float Angle2D (float x1, float x2, float y1, float y2) {
		float dx = x1 - x2;
		float dy = y1 - y2;
		float degree = Mathf.Atan2(dy,dx) * 180/Mathf.PI;
		return degree;

	}
}
