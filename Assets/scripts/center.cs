﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class center : MonoBehaviour {

	public Transform p1;
	public Transform p2;
	public Transform cam;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float X = p1.transform.position.x + ((p2.transform.position.x - p1.transform.position.x)*.5f);
		float Z = p1.transform.position.z + ((p2.transform.position.z - p1.transform.position.z)*.5f);

		Vector3 newPos = new Vector3(X, 1, Z);
		Quaternion newRot = Quaternion.Euler(0, 180-Angle2D(p1.position.x, p2.position.x, p1.position.z, p2.position.z), 0);
		
		transform.position = Vector3.Lerp(transform.position, newPos, .1f);
		transform.rotation = Quaternion.Lerp(transform.rotation, newRot, .1f);
		//transform.position = new Vector3(X, 1, Z);
		//transform.rotation = Quaternion.Euler(0, 180-Angle2D(p1.position.x, p2.position.x, p1.position.z, p2.position.z), 0);

		float distance = Distance2D(p1.position.x, p2.position.x, p1.position.z, p2.position.z);
		
		cam.transform.localPosition = Vector3.Lerp(cam.transform.localPosition, new Vector3(0,12,-20-(distance*4)), .1f);
		
	}

	float Distance2D (float x1, float x2, float y1, float y2) {
		float dx = x1 - x2;
		float dy = y1 - y2;
		return Mathf.Sqrt(dx * dx + dy * dy);
	}
	float Angle2D (float x1, float x2, float y1, float y2) {
		float dx = x1 - x2;
		float dy = y1 - y2;
		float degree = Mathf.Atan2(dy,dx) * 180/Mathf.PI;
		return degree;
	}
}
